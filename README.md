# OS and Linux intro

In this class we'll look at OS systems and mostly Linux


### OS

OS stands for operating system. They are a level of abstraction on the bare metal.

Some have GUI (graphical user interface) such as Android, Microsoft, MacOS and lots of Linux distributions. Others do not. Some are paid, others are open source (free). 

We will be using Linux a lot.

### Linux

There are many Linux distributions. Here are some: 
- Ubuntu (debian, user-friendly and used a lot)
- Fedora
- RedHad distros
- Arch
- Hali (security and penetration testing/ethical hacking)

#### Bash and shells

Linux comes with Bash terminals and they may have different shells.

Differnt shells behave different from each other. Some are:
- bash
- oh-my-zsh
- gitbash
- other

There are small differences in some commands


#### Package managers

These help install software such as python, nginx and apache. 

- RHEL = yum, dnf or rpm
- Debian = apt-get, aptitude or dpkg


#### Aspects

- Everything is classed as a file
- Everything can intereact with everything else- great for automation


#### 3 file descriptors 

To interact with programs and files, we can use, redirect and play around with 0, 1 and 2:

**0 is Standard input (stdin)**

**1 is Standard output (stdout)**

**2 isStandard error (stderr)**


### Two important paths: '/' and '~'
'/' means root directory
'~' = '/users/your_login_user/


### Environment variables

The environment is a place where code runs. In bash, we might want to set some variables. A variable is like a box. It has a name and you can add things to it. You can also open it. In bash, a variable is defined as:

```bash


MY_VAR="This is a VAr in my code" -sets a variable

echo $MY_VAR -calls the variable


MY_VAR=4895
echo $MY_VAR
> 4895        -reassigns the vairable

echo $USER
> Amalia    -an example of other variables that can  be called
```
If I close a terminal and open a new one, any variables not in the path e.g. MY_VAR will be lost.





#### Setting and defining the variables

#### Common commands

```bash
$ ENV -checks variables on terminal
$ chmod +rwx <file>
```

#### Child processses

A process that is iniciated under another process, like running another bash script.

It goes via the $PATH but is essentially a new 'terminal'.

```bash
Hence if we:
# 1. Set variable in terminal
# 2. Run the bash script that has echo $LANDLORD
./bash_file.sh
> hi from the file
>
> Lanlord above ^^

# This happens because the ./bash_file.sh runs as a child process - a new terminal
# Also because you didn't export
export LANDLORD="Jess"
./bash_file.sh
>hi from the file
>
> Landlord above^^^

# However, when you turn off your terminal, you will lose your variables.
```
To make a variable available to a child process, you need to 'export'. 
#### Path

Terminal and bash processes follow the PATH

There is a PATH for login users that have a profile and the ones without

Files to consider:
```
# These always exist at location ~
> .zprofile
> .zshrc

# for bash shell wihtout oh-my-zsh:
> .bashrc
> .bash_profile
? .bashprofile
```

#### Permissions

#### Users and sudoes

## Wild cards

### Matchers

You can use these to help you match files or content in files. 

```
# any number od character to a side
ls exam*
or 
ls *exam
or 
ls example???? - each question mark represents a character that it will display after 'exam'

## List of specific characters
ls example.[aeiout][a-z][a-zA-Z0-9]
^ Search this some more to fully understand. Not widely used.
```

### Redirect

Every command has three defaults:
- stdin   -represented by 0
- stdout  -represented by 1
- stderr  -represented by 2

Example:

```
ls
>README.md     example     example.txt

## the list that prints is stdout
```

example giving 'ls' a stdin

```bash
ls example????
> example.jpg    example.txt
# example???? is stdin and output is stdout
```


If there is an error, you get an stderr, for example:
```bash
ls example???
> zsh: no matches found
# the above is an stderr
```

You can redirect this

#### > and>>

This '>' will redirect and truncate.

This '>>' will redirect and append.

Use it with numbers to decide what to redirect.

#### Messing with stdin






### Pipes

Piping redirects and makes it stdin of another program. 